
"""
---------- Dedicated to Mary our mother in spirit, and to her son Jesus our brother in spirit.
---------- Just like Adam and Eve. A new beginning...
"""

import sys
sys.path.append('./libs')
import xml2gui as g
import subprocess
import os


current_page = 1


def get_path(page):
    return "res/-" + str(page).rjust(4, '0') + ".png"


def show_page(page):
    global current_page
    file = get_path(page)
    if os.path.exists(file):
        current_page = page
        g.mcxml_set_src("imgWidget", file)
        g.mcxml_set()
    else:
        pdf = 'res/Original-Douay-Rheims-Bible-Merged.pdf'
        cmd = "poppler/pdftoppm.exe -png -hide-annotations -f {} -l {} -progress {} res/ ".format(page, page, pdf)
        p = subprocess.Popen(cmd)
        r = p.wait()
        if r == 0:
            current_page = page
            g.mcxml_set_src("imgWidget", file)
            g.mcxml_set()
        else:
            print("Image generation failed")


def listener_click(id, msg):
    if msg == "up":
        if id == "prev":
            show_page(max(1, current_page-1))
        elif id == "next":
            show_page(min(2294, current_page+1))


def listener_drag(id, msg):
    if id == "imgWidget":
        list = msg.split("|")
        g.mcxml_set_left(id, list[1])
        g.mcxml_set_top(id, list[2])
        g.mcxml_set()


def listener_started():
    show_page(current_page)
    

def listener_stopped():
    pass


if __name__ == '__main__':
    xml = open('app.xml', mode='r', encoding='utf-8').read()
    g.mcxml_listener_drag(listener_drag)
    g.mcxml_listener_click(listener_click)
    g.mcxml_started(listener_started)
    g.mcxml_stopped(listener_stopped)
    g.mcxml_loop(xml)
    
